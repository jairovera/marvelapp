//
//  Pokemon.swift
//  marvelApp
//
//  Created by Jairo Vera on 29/11/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import Foundation
import ObjectMapper

class Pokemon: Mappable {
    var name: String?
    var height:Double?
    var weight: Double?
    
    required init?(map: Map){
        
    }
    func mapping(map: Map){
        name<-map["name"]
        height<-map["height"]
        weight<-map["weight"]
    }
}
